from django.db import models
from django.contrib.auth.models import User


class Todo(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name='todos')
    title = models.CharField(max_length=200, blank=True)
    description = models.CharField(max_length=200, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    updated = models.DateTimeField(auto_now=True, blank=True)
    priority = models.CharField(max_length=32, default='low', blank=True)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return self.title
