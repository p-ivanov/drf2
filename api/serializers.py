from django.contrib.auth.models import User
from .models import Todo
from rest_framework import serializers


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ['title', 'description', 'priority',
                  'created', 'updated', 'is_done']
        read_only_fields = ('created', 'url')


class UserSerializer(serializers.ModelSerializer):
    todos = TodoSerializer(many=True, required=False)

    class Meta:
        model = User
        fields = ['url', 'username', 'first_name', 'last_name',
                  'password', 'is_active', 'is_staff', "email", 'todos']
        read_only_fields = ('is_active', 'is_staff', 'url')
        extra_kwargs = {
            'password': {'write_only': True},
        }
