from django.contrib.auth.models import AnonymousUser, User, Group
from django.db.models import query
from django.http import request
from django.http.response import JsonResponse
from .serializers import *
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action, permission_classes
from rest_framework import status


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @action(detail=True, methods=['get'])
    def todos(self, request, pk):

        if request.user == self.get_object():
            user = User.objects.all()
            instance = self.get_object()
            serializer_class = UserSerializer(
                instance, context={'request': request})
            return Response(serializer_class.data['todos'])
        else:
            return Response({"API_Response": "Not your property"})

    def get_queryset(self):
        qs = User.objects.all()
        return qs

    def retrieve(self, request, *args, **kwargs):
        if request.user == self.get_object():
            queryset = User.objects.all()
            instance = self.get_object()
            serializer_class = UserSerializer(
                instance, context={'request': request})

            return Response(serializer_class.data)
        else:
            return Response({"API_Response": "Not your property"})


class TodoViewSet(viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class = TodoSerializer

    def get_queryset(self):
        qs = Todo.objects.all().order_by('-created')
        return qs

    def retrieve(self, request, *args, **kwargs):
        queryset = Todo.objects.all()
        instance = self.get_object()
        serializer_class = TodoSerializer(
            instance, context={'request': request})

        return Response(serializer_class.data)


""" class TasksViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class = TasksSerializer

    ''' [method=GET] tasks/ '''

    def get_queryset(self):
        queryset = Task.objects.all()
        serializer_class = TasksSerializer
        return queryset

    ''' [method=GET] tasks/task_id '''

    def retrieve(self, request, *args, **kwargs):
        queryset = Task.objects.all()
        instance = self.get_object()
        serializer_class = SingleTaskSerializer(instance)
        print("IN tasks/id...query_params:", request.query_params)
        print("IN tasks/id...headers:", request.headers)
        print("IN tasks/id...body:", request.body)
        return Response(serializer_class.data)

 """


class Hello(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    parser_classes = [JSONParser]

    def get(self, request):
        print("request.data:", request.data)
        print("request.user:", request.user)
        if(request.data.get('key') == "testvalue"):
            print("Your key is 'testvalue'")
        else:
            print("Your key is not 'testvalue'")
        return Response({'GET_BODY(data)': request.data, "GET_PARAMS:": request.query_params})

    def post(self, request):
        print("request.data:", request.data)
        print("request.user:", request.user)
        return Response({"POST_BODY(data)": request.data, "POST_PARAMS:": request.query_params})


class TokenNeeded(APIView):
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [JSONParser]

    def get(self, request):
        return Response({"API_Response": "Token needed to see this response. I Guess you have an active token :)"})


class NoTokenNeeded(APIView):
    permission_classes = [permissions.AllowAny]
    parser_classes = [JSONParser]

    def get(self, request):
        return Response({"API_Response": "No token needed to see this response."})

class Test(APIView):
    permission_classes = [permissions.AllowAny]
    parser_classes = [JSONParser]

    def get(self, request):
        return Response({"API_Response": "Test response"})
